import string
import random


class LoginPage():
    driver: object

    def __init__(self, driver):
        self.driver = driver

        self.create_account_email = "/html/body/div/div[2]/div/div[3]/div/div/div[1]/form/div/div[2]/input"
        self.create_account_btn = "/html/body/div/div[2]/div/div[3]/div/div/div[1]/form/div/div[3]/button/span/i"

        self.registered_email_xpath = "/html/body/div/div[2]/div/div[3]/div/div/div[2]/form/div/div[1]/input"
        self.registered_password_xpath = "/html/body/div/div[2]/div/div[3]/div/div/div[2]/form/div/div[2]/span/input"
        self.registered_login_btn = "/html/body/div/div[2]/div/div[3]/div/div/div[2]/form/div/p[2]/button/span"
        self.registered_password = "abrakadabra"
        self.registered_email = "alexartem27@gmail.com"
        self.new_email = self.generate_email(15)

    def generate_email(self,num=15):
        letters = 'abcdefghijklmnopqrstuvwxyz0123456789'
        generated_login = ''.join((random.choice(letters) for i in range(1, num)))
        return generated_login

    a = generate_email(15)
    b = a + '@gmail.com'
    print(a, b)

    def fill_account_email(self):
        self.driver.find_element_by_xpath(self.create_account_email).send_keys(LoginPage.b)

    def create_account_btn_click(self):
        self.driver.find_element_by_xpath(self.create_account_btn).click()

    # already registered user login
    def enter_registered_email(self):
        driver = self.driver
        driver.find_element_by_xpath(self.registered_email_xpath).send_keys(self.registered_email)

    def enter_registered_password(self):
        driver = self.driver
        driver.find_element_by_xpath(self.registered_password_xpath).send_keys(self.registered_password)

    def registered_sign_in_click(self):
        self.driver.find_element_by_xpath(self.registered_login_btn).click()
