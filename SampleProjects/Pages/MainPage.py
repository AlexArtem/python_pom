from selenium import webdriver


class MainPage():

    def __init__(self, driver):
        self.driver = driver

        self.logo = "/html/body/div/div[1]/header/div[3]/div/div/div[1]/a/img"
        self.login_btn = "/html/body/div/div[1]/header/div[2]/div/div/nav/div[1]/a"

    def click_sign_in(self):
        self.driver.find_element_by_xpath(self.login_btn).click()
