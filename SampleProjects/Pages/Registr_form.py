class Registr_form():

    def __init__(self, driver):
        # your personal info block
        self.driver = driver
        self.mr_radio_btn = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[1]/div[1]/label'
        self.mrs_radio_btn = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[1]/div[2]/label'
        self.first_name = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[1]/div[2]/label'
        self.last_name = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[3]/input'
        self.autofilled_email = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[4]/input'
        self.password = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[5]/input'
        self.date_of_birth = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[6]/div/div[1]/div/select'
        self.month_of_birth = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[6]/div/div[2]/div/select'
        self.year_of_birth = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[6]/div/div[3]/div/select'
        self.signUp_newsletter_checkbox = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[7]/div/span/input'
        self.receive_spec_offers = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[8]/div/span/input'

        # your address block
        self.autofilled_first_name = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[1]/input'
        self.autofilled_second_name = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[2]/input'
        self.company = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[3]/input'
        self.address = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[4]/input'
        self.address2 = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[5]/input'
        self.city = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[6]/input'
        self.state = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[7]/div/select'
        self.zip_code = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[8]/input'
        self.country = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[9]/div/select'
        self.add_info = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[10]/textarea'
        self.home_phone = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[12]/input'
        self.mobile_phone = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[13]/input'
        self.my_address = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[14]/input'
        self.register_btn = '/html/body/div/div[2]/div/div[3]/div/div/form/div[4]/button/span'

    def mr_radio_click(self):
        self.driver.find_element_by_xpath(self.mr_radio_btn).click()
        return self

    def mrs_radio_click(self):
        self.driver.find_element_by_xpath(self.mrs_radio_btn).click()
        return self

    def fill_first_name(self):
        self.driver.find_element_by_xpath(self.first_name).send_keys('Alex')
        return self

    def fill_last_name(self):
        self.driver.find_element_by_xpath(self.last_name).send_keys('Artem')
        return self
